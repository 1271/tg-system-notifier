
#[macro_export]
macro_rules! app_info {
    () => {
        app_info!(' ')
    };
    (name) => {
        env!("CARGO_PKG_NAME")
    };
    (version) => {
        env!("CARGO_PKG_VERSION")
    };
    ($delimiter: literal) => {
        concat!(app_info!(name), $delimiter, app_info!(version))
    };
}

#[macro_export]
macro_rules! errln_red {
    ($($arg:tt)+) => {
        eprintln!("\x1b[41m{}\x1b[0m", format_args!($($arg)*))
    };
}