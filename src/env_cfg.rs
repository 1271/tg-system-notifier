use std::env;
use crate::app_info;
use std::io::Write;
use clap::builder::TypedValueParser;

pub(crate) fn logger_init() {
    match env::var("RUST_LOG_STYLE") {
        Ok(s) if s.to_uppercase() == "SYSTEMD" => env_logger::builder()
            .format(|buf, record| {
                writeln!(
                    buf,
                    "<{}>{}: {}",
                    match record.level() {
                        log::Level::Error => 3,
                        log::Level::Warn => 4,
                        log::Level::Info => 6,
                        log::Level::Debug => 7,
                        log::Level::Trace => 7,
                    },
                    record.target(),
                    record.args()
                )
            })
            .init(),
        _ => env_logger::init(),
    };
}

pub fn env_args() -> clap::ArgMatches {
    // bold text
    // format!("\x1b[1m{}\x1b[0m", app_info!())
    /*.arg(
        clap::Arg::new("init")
            .required(false)
            .num_args(0)
            .action(clap::ArgAction::SetTrue)
            .help("Initialize config")
    )*/
    clap::builder::Command::new(app_info!(name))
        .arg(
            clap::Arg::new("config")
                .short('c')
                .long("config")
                .required(true)
                .value_parser(ConfigParamParser {})
                .num_args(1)
                .help("Config file"),
        )
        .version(app_info!(version))
        .help_template(
            "{before-help}{about-with-newline}{usage-heading} {usage}\n{all-args}{after-help}",
        )
        .before_long_help(
            "Telegram notifier daemon for your server.",
        )
        .get_matches()
}

#[derive(Clone)]
struct ConfigParamParser;

impl TypedValueParser for ConfigParamParser {
    type Value = String;

    fn parse_ref(
        &self,
        _cmd: &clap::Command,
        _arg: Option<&clap::Arg>,
        value: &std::ffi::OsStr,
    ) -> Result<Self::Value, clap::Error> {
        match value.to_str() {
            Some(v) if v.ends_with(".toml") => Ok(v.to_string()),
            _ => {
                log::error!("Bad argument: {:?}", value);
                Err(clap::error::Error::new(
                    clap::error::ErrorKind::InvalidValue,
                ))
            }
        }
    }
}

