use teloxide::utils::command::BotCommands;

#[derive(BotCommands, Clone)]
#[command(
rename_rule = "identity",
description = "These commands are supported:"
)]
pub enum TgCommand {
    /// Display this text.
    Help,
    // /// Handle a username.
    // Username(String),
}

#[derive(BotCommands, Clone)]
#[command(rename_rule = "identity", description = "Md raid commands:")]
pub enum MdCommand {
    /// Display info about md raids.
    MdInfo,
}

#[derive(BotCommands, Clone)]
#[command(rename_rule = "identity", description = "Configuration bot commands:")]
pub enum ConfigCommand {
    ///
    Config,
}
