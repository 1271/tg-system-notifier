use std::borrow::Cow;

use serde::{Deserialize, Serialize};
use teloxide::types::UserId;
use validator::{Validate, ValidationError, ValidationErrors, ValidationErrorsKind};

use crate::errln_red;

// #[command(alias = "h")]
// #[command(alias = "u")]
// #[command(parse_with = "split", alias = "ua", hide_aliases)]

//https://serde.rs/container-attrs.html
#[derive(Clone, Deserialize, Serialize, Debug)]
#[serde(untagged)]
pub enum ConfigUserEnum {
    Username(String),
    Id(u64),
}

#[derive(Clone, Validate, Deserialize, Serialize, Debug)]
#[validate(schema(function = "config_validator_fn"))]
pub struct Config {
    #[validate]
    pub telegram: ConfigTelegram,
    #[validate]
    pub mdadm: Option<ConfigMdAdm>,
    #[validate]
    pub disk: Option<Vec<ConfigDiskSpace>>,
}

#[derive(Clone, Validate, Deserialize, Serialize, Debug)]
pub struct ConfigTelegram {
    #[validate]
    pub token: String,
    #[validate]
    pub admin: ConfigUserEnum,
    #[validate]
    pub users: Vec<ConfigUserEnum>,
}

#[derive(Clone, Validate, Deserialize, Serialize, Debug)]
pub struct ConfigMdAdm {
    pub devices: Vec<String>, // md0, md1, ...
}

#[derive(Clone, Validate, Deserialize, Serialize, Debug)]
pub struct ConfigDiskSpace {
    pub path: String,
    #[validate(range(min = 1, max = 99))]
    pub min_percent: Option<u8>,
    #[validate(range(min = 1, max = 99))]
    pub max_percent: Option<u8>,
    #[validate(range(min = 1))]
    pub min_mb: Option<u32>,
    #[validate(range(min = 1))]
    pub max_mb: Option<u32>,
}

impl Config {
    pub fn validate_config(&self) -> Result<(), ()> {
        match self.clone().validate() {
            Ok(_) => Ok(()),
            Err(ValidationErrors(errors)) => {
                errln_red!("Found some errors:");
                errors.iter().for_each(|(_, err)| {
                    match err {
                        ValidationErrorsKind::Field(fields) => fields.iter().for_each(|f| {
                            if let Some(m) = f.message.clone() {
                                if f.code.len() > 0 {
                                    errln_red!(" > {}: {}", f.code, m.to_string(),)
                                } else {
                                    errln_red!(" > {}", m.to_string())
                                }
                            } else {
                                errln_red!("{}", f.code)
                            }
                        }),
                        ValidationErrorsKind::List(items) => items.iter().for_each(|(_, err)| {
                            errln_red!(" > {}", err.to_string());
                        }),
                        ValidationErrorsKind::Struct(err) => {
                            errln_red!(" > {}", err.to_string())
                        }
                    };
                });

                Err(())
            }
        }
    }
}

impl ConfigTelegram {
    pub fn check_access(&self, user: teloxide::types::User) -> bool {
        if check_user(self.admin.clone(), user.clone()) {
            true
        } else {
            for u in self.users.clone() {
                if check_user(u, user.clone()) {
                    return true;
                }
            }

            false
        }
    }

    // pub fn is_admin(&self, user: teloxide::types::User) -> bool {
    //     check_user(self.admin.clone(), user.clone())
    // }
}

impl ConfigDiskSpace {
    pub fn validate_limits(&self) -> Result<(), &str> {
        if self.is_limits_valid() && self.is_percents_valid() && self.is_mb_valid() {
            Ok(())
        } else {
            Err("Please configure one of limits")
        }
    }

    fn is_limits_valid(&self) -> bool {
        self.min_percent.is_some()
            || self.max_percent.is_some()
            || self.min_mb.is_some()
            || self.max_mb.is_some()
    }

    fn is_percents_valid(&self) -> bool {
        is_min_max_valid(self.min_percent.clone(), self.max_percent.clone())
    }

    fn is_mb_valid(&self) -> bool {
        is_min_max_valid(self.min_mb.clone(), self.max_mb.clone())
    }
}

fn config_validator_fn(cfg: &Config) -> Result<(), ValidationError> {
    // fixme
    if false && cfg.disk.is_none() && cfg.mdadm.is_none() {
        return Err(ValidationError::new("").with_message(Cow::Borrowed(
            "Please configure one of the checks (disk/mdadm/etc)",
        )));
    }

    if let Some(disks) = cfg.disk.clone() {
        for d in disks {
            if let Err(e) = d.validate_limits() {
                return Err(ValidationError::new("disk.*.<min_max>")
                    .with_message(Cow::Owned(e.to_string())));
            }
        }
    }

    Ok(())
}

fn is_min_max_valid<I: PartialOrd>(min: Option<I>, max: Option<I>) -> bool {
    match (min, max) {
        (Some(min), Some(max)) => min < max,
        _ => true,
    }
}

fn check_user(cfg_user: ConfigUserEnum, tg_user: teloxide::types::User) -> bool {
    match cfg_user {
        ConfigUserEnum::Username(name) => {
            if let Some(u) = tg_user.username {
                name == u
            } else {
                false
            }
        }
        ConfigUserEnum::Id(uid) => {
            let UserId(tg_uid) = tg_user.id;
            uid == tg_uid
        }
    }
}