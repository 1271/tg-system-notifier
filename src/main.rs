mod app_macro;
mod command;
mod config;
mod env_cfg;
mod handlers;

use std::alloc::System;
use std::fs::File;
use std::io::Read;

// use regex::Regex;
use teloxide::prelude::*;

#[global_allocator]
static A: System = System;

#[tokio::main(flavor = "multi_thread")]
async fn main() -> Result<(), ()> {
    env_cfg::logger_init();

    let args = env_cfg::env_args();
    let cfg_path = args.get_one::<String>("config").unwrap();

    if let Ok(mut f) = File::open(cfg_path) {
        let buf = &mut vec![];
        f.read_to_end(buf).expect("Unexpected EOF");
        let content = match String::from_utf8(buf.to_vec()) {
            Ok(o) => o,
            Err(e) => {
                errln_red!("Unexpected config file content");
                log::debug!("{:?}", e.as_bytes());

                // BREAK
                return Err(());
            }
        };

        let config: config::Config = match toml::from_str(&content) {
            Ok(o) => o,
            Err(e) => {
                errln_red!("Config file corrupted?\n{}", e.to_string());

                // BREAK
                return Err(());
            }
        };

        if config.validate_config().is_err() {
            // BREAK
            return Err(());
        }

        let bot = Bot::new(config.telegram.token.clone());

        let handler = Update::filter_message()
            .branch(
                dptree::entry()
                    .filter_command::<command::TgCommand>()
                    .endpoint(handlers::base_commands_handler),
            )
            .branch(
                dptree::entry()
                    .filter_command::<command::MdCommand>()
                    .endpoint(handlers::md_commands_handler),
            );

        Dispatcher::builder(bot, handler)
            .dependencies(dptree::deps![
                config.telegram.clone(),
                config.mdadm.clone(),
                config.disk.clone()
            ])
            // If no handler succeeded to handle an update, this closure will be called.
            .default_handler(|upd| async move {
                log::warn!("Unhandled update: {:?}", upd);
            })
            // If the dispatcher fails for some reason, execute this handler.
            .error_handler(LoggingErrorHandler::with_custom_text(
                "An error has occurred in the dispatcher",
            ))
            .enable_ctrlc_handler()
            .build()
            .dispatch()
            .await;
        Ok(())
    } else {
        errln_red!("Cannot read config file! Please, check permissions.");
        Err(())
    }
}

#[derive(Clone)]
struct StateValues {
    cfg_path: String,
    cfg: config::Config,
}
