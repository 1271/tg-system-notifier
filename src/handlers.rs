use crate::config;
use teloxide::{prelude::*, utils::command::BotCommands};
use crate::command;

pub async fn base_commands_handler(
    cfg: config::ConfigTelegram,
    bot: Bot,
    // me: teloxide::types::Me,
    msg: Message,
    cmd: command::TgCommand,
) -> Result<(), teloxide::RequestError> {
    let text = match msg.from() {
        Some(sender) if cfg.check_access(sender.clone()) => match cmd {
            command::TgCommand::Help => {
                let user_info = user_id_with_name_if_possible(sender.clone());
                log::info!("/help from {}", user_info);
                format!(
                    "Your id: {}\n\n{}\n\n{}",
                    user_info,
                    command::TgCommand::descriptions(),
                    command::MdCommand::descriptions(),
                )
            }
        },
        _ => {
            // user not have bot commands access
            format!("Access denied")
        }
    };

    bot.send_message(msg.chat.id, text).await?;

    Ok(())
}

pub async fn md_commands_handler(
    cfg_tg: config::ConfigTelegram,
    cfg_md: Option<config::ConfigMdAdm>,
    bot: Bot,
    msg: Message,
    cmd: command::MdCommand,
) -> Result<(), teloxide::RequestError> {
    let text = match msg.from() {
        Some(sender) if cfg_tg.check_access(sender.clone()) => {
            match cmd {
                command::MdCommand::MdInfo => {
                    log::info!(
                        "/mdinfo from {}",
                        user_id_with_name_if_possible(sender.clone())
                    );

                    match cfg_md {
                        Some(_cfg) => {
                            // TODO
                            "todo".to_string()
                        }
                        _ => {
                            format!("Config does not contain information about raids")
                        }
                    }
                }
            }
        }
        _ => {
            // user not have bot commands access
            format!("Access denied")
        }
    };

    bot.send_message(msg.chat.id, text).await?;

    Ok(())
}

fn user_id_with_name_if_possible(user: teloxide::types::User) -> String {
    let UserId(sender_id) = user.id.clone();
    match user.username.clone() {
        Some(u) => format!("<{}>: {}", sender_id, u),
        None => format!("<{}>", sender_id),
    }
}
